<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>README</title>
</head>
<body>
    <h1>Presto.it</h1>    
    <h2>The final project I worked on for the graduation of the coding bootcamp AULAB, for Full Stack Web Developers</h2>
    <p>This whole website was done in two weeks, using two-day sprints and the Pomodoro method

        We worked in Laravel 8 Framework, using PHP
        
        For the front end layout, we used HTML, CSS, Bootstrap Javascript and jQuery
        
        <strong>Back end functionalities</strong> 
        
        We used Laravel Fortify for the authentication / registration, also added the contact option via mail set-up.
        
        The website is one filled with ads uploaded by a user, that's why we also created a Revisor role functionality
        
        You can use the search bar to find all ads within the website and you can also filter them by category.
        
        Dropzone was implemented in order to grant the possibility to add more than one photo
        
        Eventually, we installed Google Vision APIs and with it, we resized the images uploaded by a user, we can also add a watermark. Also, thanks to Google Vision AI we can check if the images uploaded have sensitive or offensive content and we can also blur faces</p>
</body>
</html>